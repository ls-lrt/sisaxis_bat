#!/usr/bin/perl -w

use strict;

my $sys_power_dir = "/sys/class/power_supply";
my %controller = ();

sub path {
    return join('/', @_);
}

sub path_walker {
    my $path = shift;
    my $cb = shift;
    opendir(my $d, $path) or die "Failed to open '$path': $!";
    while (readdir($d)) {
	$cb->($_) if (defined $cb);
    }
    closedir($d);
}

sub file2str {
    my $path = shift;
    my $cb = shift;
    my $str = "";
    open(my $fd, "<", $path) or die "Failed to open '$path': $!";
    while (<$fd>) {
	$cb->($_) if (defined $cb);
	$str .= $_;
    }
    close($fd);
    return $str;
}

sub get_sysfs_entries {
    my $ctrl = shift;
    my $that = $controller{$ctrl};
    path_walker($that->{sys_power_path}, sub {
	my $f = $_;
	my $path = path($that->{sys_power_path}, $f); 
	if ( -r $path && ! -l $path && ! -d $path) {
	    $that->{$f} = file2str($path, sub {chomp;});
	}
		});
}

sub init_list_controller() {
    path_walker($sys_power_dir, sub {
	if ( /sony_controller_battery/ ) {
            my ($hw_addr) = /sony_controller_battery_(([\da-f]{2}:?){6})/;
            $controller{$hw_addr}{sys_power_path} = path($sys_power_dir, "sony_controller_battery_$hw_addr");
	    get_sysfs_entries($hw_addr);
	}
		});
}

sub get_term_info {
    my $i = shift || 0;
    my @info = split(/;/, (qx(stty -a))[0]);
    return $info[$i];
}

sub screen_h() {
    return ((get_term_info(1))[-1] =~ /(\d+)/)[0];
}

sub screen_w() {
    return ((get_term_info(2))[-1] =~ /(\d+)/)[0];
}

sub p_blank {
    my $l = shift || 0;
    print join(" ", ("") x $l);
}

sub hline {
    my $delim = shift || '-';
    my $start = shift || 0;
    my $length = shift;
    my $percent = shift || 42;
    $length = (screen_w - $start) if (! defined $length);

    if ($length > 5) {
	$length = $length - 5;
    }
    p_blank($start);
    print join($delim, ("") x ($length / 2));
    printf " %3d%% ", $percent;
    print join($delim, ("") x ($length / 2));
}

sub draw_battery {
    my $hw_addr = shift || "00:11:22:33:44:55";
    my $percent_fill = shift || 42;
    my $charging = shift || 0;
    my $c = shift || '#';
    $hw_addr .= " |";
    my $level = (screen_w - length($hw_addr)) * $percent_fill / 100;

    print "$hw_addr";
    if ($charging) {
	print "\033[0;34m"; # blue
    } else {
	if ($percent_fill < 25) {
	    print "\033[0;31m"; # red
	} elsif ($percent_fill < 50) {
	    print "\033[0;33m"; # yellow
	} else {
	    print "\033[0;32m"; # green
	}
    }
    hline($c, 0, $level, $percent_fill);
    print "\033[0m";
    print "\n";
}

init_list_controller;

for (keys %controller) {
#type
#status
#capacity
#scope
#present
    if ($controller{$_}{present}) {
	draw_battery($_, $controller{$_}{capacity}, ($controller{$_}{status} =~ /Charging/));
    }
}
