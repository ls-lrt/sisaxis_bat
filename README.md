# Small script to display battery level of sisaxis controller

When sysaxis devices are connected using bluetooth, this small script
scan sysfs and display battery level for each device found.

I personally use it with watch:
```
    watch --color -n30 ./bat.pl
```

# Small helper to connect a new device using bluez

Quoting this good ressource:
https://wiki.gentoo.org/wiki/Sony_DualShock

* Start bluetoothctl:
```
user$ bluetoothctl
```
* Enable the agent and set it as default:
```
[bluetooth]# agent on
[bluetooth]# default-agent
```
* Power on the Bluetooth controller, and set it as discoverable and
pairable:
```
[bluetooth]# power on
[bluetooth]# discoverable on
[bluetooth]# pairable on
```
* Connect the DualShock 3 to the system using a USB cable and press the
PlayStation button.
* Discover the DualShock 3 MAC address:
```
[bluetooth]# devices
```
* Disconnect the USB cable from the DualShock 3:
```
Allow the service authorization request:
[agent]Authorize service service_uuid (yes/no): yes
```
* Trust the DualShock 3:
```
[bluetooth]# trust device_mac_address
```
* The DualShock 3 is now paired:
```
[bluetooth]# quit
```
